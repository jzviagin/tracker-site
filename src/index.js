import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import thunk from 'redux-thunk'
import authReducer from './store/reducers/authReducer'
import mainReducer from './store/reducers/mainReducer'
import {Provider} from 'react-redux'
//import fff from './screens/bbb'

import C from './screens/c'

import {createStore, applyMiddleware, compose, combineReducers} from 'redux';



const rootReducer = combineReducers({
    auth: authReducer,
    main: mainReducer
})




const composeEnhancers = (process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null )|| compose;



const store = createStore(rootReducer , composeEnhancers(applyMiddleware( thunk)));


const MainApp = ()=>{

    
    return (
        <Provider store = {store}>
            <App/>
        </Provider>
    )

}


ReactDOM.render(<MainApp />, document.getElementById('root'));
registerServiceWorker();
