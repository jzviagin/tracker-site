import React, { createContext, useState, useContext, useMemo, useCallback } from 'react';

const initialState = {
   count: 0,
   increment: undefined,
   decrement: undefined,
   hello: 'Hello world',
};
const CounterContext = createContext(initialState);
const CounterProvider = ({ children }) => {
    console.log('provider render')
   const [count, setCount] = useState(0);
   const [hello] = useState('Hello world');
   const increment = useCallback (() => setCount((counter) => counter + 1),[]);
   const decrement = useCallback (() => setCount((counter) => counter - 1),[]);
   const value = {
       count,
       increment,
       decrement,
       hello,
   };
   return  <CounterContext.Provider value={value}>{children}</CounterContext.Provider>;
};
const SayHello = () => {
   const { hello } = useContext(CounterContext);
   console.log('[SayHello] rendered');
return  useMemo(()=> {
    console.log('[SayHello] rendered 2');
    return <h1>{hello}</h1>
},[hello]);
};
const IncrementCounter = () => {
   const { increment } = useContext(CounterContext);
   console.log('[IncrementCounter] rendered');
   return <button onClick={increment}> Increment</button>;
};
const DecrementCounter = () => {
   console.log('[DecrementCounter] rendered');
   const { decrement } = useContext(CounterContext);
   return  useMemo( ()=>{
    console.log('[DecrementCounter] rendered3');
       return <button onClick={decrement}> Decrement</button>;
   }, [decrement]);

    
};
const ShowResult = () => {
   console.log('[ShowResult] rendered');
   const { count } = useContext(CounterContext);
   return <h1>{count}</h1>;
};
const App = () => (
   <CounterProvider>
       <SayHello />
       <ShowResult />
       <IncrementCounter />
       <DecrementCounter />
   </CounterProvider>
);
export default App;
