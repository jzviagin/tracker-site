import React  from 'react'
import classes from './SearchScreen.css'
import {connect} from 'react-redux'
import withMainStore from '../hooks/withMainStore'
import UserDetailsSmall from '../Components/UserDetailsSmall'

class SearchScreen extends React.Component{

    constructor(){
        super()
    }
    render(){

        const userIds = this.props.getUsers()

        return <div className = {classes.Main}>
            <input value = {this.props.searchTerm} type="text" onChange = {
                        (event)=>{
                            this.props.searchUsers(event.target.value)
                            
                        }
                    } id="term" name="fname" placeholder = "Please enter a user name"/>
                <ul>
                {userIds.map( (id)=>{
                    const user = this.props.getUser(id)
                    return(
                    <li>
                        <UserDetailsSmall user = {user}></UserDetailsSmall>
                    </li>)
                })}
                </ul>

        </div>
    }
}




export default  withMainStore(SearchScreen);