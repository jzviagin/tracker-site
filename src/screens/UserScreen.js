import React from 'react';
import {connect} from 'react-redux'
import * as authActionCreators from '../store/actions/authActions'
import * as mainActionCreators from '../store/actions/mainActions'
import loading from '../assets/images/loading.gif'
import classes from './UserScreen.css'
import arrow from '../assets/images/arrow.png'
import userPhoto from '../assets/images/user.png'
import {NavLink} from 'react-router-dom'

import AuthImage from '../Components/AuthImage'
import UserList from '../Components/UserList'
import UserTracks from '../Components/UserTracks'
import { faUserPlus, faUserMinus } from '@fortawesome/free-solid-svg-icons'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'




import withMainStore from '../hooks/withMainStore'


class UserScreen extends React.Component{

    constructor(){
        super();
        //this.testClick = this.testClick.bind(this)
     

    }


    componentDidMount(){
        this.props.closeModal();
    }

    componentDidUpdate(prevProps) {
        if (this.props.userId !== prevProps.userId) {
          this.props.closeModal();
        }
      }


   

    testClick = ()=>{
        console.log('test click' ,this )
    }



    render(){

        console.log('user screen props', this.props)

        const user = this.props.getUser(this.props.userId);


        if (!user ){
            return (
                <div className = {classes.ActivityIndicator} >
                    <img src = {loading}/>
                </div>
            )
        }


        console.log('user screen props', this.props)

        const following = this.props.getFollowing(user._id)

        const followingUsers = following.map( (id)=>{
            return this.props.getUser(id);
        });

        const followers = this.props.getFollowers(user._id)

        let followed = false;
        for (let i = 0 ; i < followers.length; i++){
            if (followers[i] == this.props.authUser._id){
                followed = true;
            }
        }

        const followerUsers = followers.map( (id)=>{
            return this.props.getUser(id);
        });


        console.log('user screen following users', followingUsers)



/**
 * 
 * 
 *                     <NavLink className= {classes.NavLink} >{"following"}</NavLink>
                    <NavLink className= {classes.NavLink} >{"followers"}</NavLink>
                    <NavLink className= {classes.NavLink} >{"tracks"}</NavLink>
 * 
 * 
 */

 

     

        return (
            <div className={classes.Main}>
                <h1>{user.username}</h1>
                <AuthImage className = {classes.UserImage} src = {user.photo} onError= {(param)=>{
                        console.log('image error', param.target)
                        param.target.src = userPhoto
                    }}></AuthImage>
                <h1>{user.firstName + " " + user.lastName}</h1>

                <a className = {classes.MailLink} href={"mailto:" + user.email}   >{user.email}</a>


                {user._id != this.props.authUser._id?<button className = {classes.FollowButton} onClick = {()=>{
                        if (followed){
                            this.props.unfollow(this.props.userId)
                        }else{
                            this.props.follow(this.props.userId)
                        }
                        
                    }}><FontAwesomeIcon width = {50} className = {classes.FollowIcon} icon={followed == false  ?faUserPlus: faUserMinus} />
                </button>:null}
            
                <div className={classes.Links}>
                    <a to="\user" onClick={()=>{this.props.showModal(<UserList title = {"Following"} users = {followingUsers}/>)}} className= {classes.NavLink} >{"Following"}</a>
                    <a to="\user" onClick={()=>{this.props.showModal(<UserList title = {"Followers"} users = {followerUsers}/>)}} className= {classes.NavLink} >{"Followers"}</a>
                    <a to="\user" onClick={()=>{this.props.showModal(<UserTracks  showModal = {this.props.showModal} closeModal = {this.props.closeModal} userId = {this.props.userId} title = {"Tracks"} />)}} className= {classes.NavLink} >{"Tracks"}</a>
                </div>

            </div>
        )
    }
}

const mapStateToProps = (state)=>{
    console.log('mapStateToProps', this.props)
    return {
        authUser: state.auth.user,
        inProgress: state.auth.inProgress,
        errorMessage: state.auth.errorMessage
    }
}

const mapDispatchToProps = (dispatch)=>{
    return {
        onSignUp: (data) => {dispatch(authActionCreators.signup(data))},
    }
}

 export default connect(mapStateToProps, mapDispatchToProps)( withMainStore(UserScreen) );