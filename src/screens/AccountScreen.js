import React from 'react'
import { ACTION_CLEAR_SEARCH_USERS } from '../store/actions/actionTypes';
import classes from './AccountScreen.css'
import { connect } from 'react-redux';
import UserScreen from './UserScreen';
import withMainStore from '../hooks/withMainStore';
import { faUserPlus, faUserMinus, faEdit } from '@fortawesome/free-solid-svg-icons'
import { withRouter } from 'react-router';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class AccountScreen extends React.Component{
    render(){
        console.log('account screen props' ,this.props)
        return <div className = {classes.Main}>
            <UserScreen userId = {this.props.authUser._id} showModal = {this.props.showModal} closeModal = {this.props.closeModal}/>
            <button className = {classes.EditButton} onClick = {()=>{
                       this.props.history.push('/editAccount')
                        
                    }}><FontAwesomeIcon width = {50} className = {classes.EditIcon} icon={faEdit} />
                </button>
        </div>
    }
}



const mapStateToProps = (state)=>{
    console.log('mapStateToProps', this.props)
    return {
        authUser: state.auth.user,
        inProgress: state.auth.inProgress,
        errorMessage: state.auth.errorMessage
    }
}

const mapDispatchToProps = (dispatch)=>{
    return {
        
    }
}


export default  withRouter(connect(mapStateToProps, mapDispatchToProps)(withMainStore(AccountScreen)));