import React from 'react';
import {connect} from 'react-redux'
import * as authActionCreators from '../store/actions/authActions'
import loading from '../assets/images/loading.gif'
import classes from './LoginScreen.css'
import arrow from '../assets/images/arrow.png'
import {NavLink} from 'react-router-dom'


class LoginScreen extends React.Component{

    constructor(){
        super();
        this.state = {
            inputsValid:false,
            email: '',
            password: ''
        }

    }


    componentDidMount(){

    }




    render(){

        console.log('login screen state', this.state)

        if (this.props.inProgress){
            return (
                <div className = {classes.ActivityIndicator} >
                    <img src = {loading}/>
                </div>
            )
        }

        const checkEmail = (email)=>{
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            return pattern.test(email);
        }

        const checkPassword = (password)=>{
            return password.length !== 0;
        }

        const checkValidity = ()=>{
            const email = document.getElementById("email").value ;
            const password = document.getElementById("password").value ;
            if( checkEmail(email)== false){
                return false;
            }
            if( checkPassword(password) == false){
                return false;
            }
            return true;
        }

    

        const submitForm = ()=>{
            console.log("submit!!!!!!!!!!!!!!!")
            const email = document.getElementById("email").value ;
            const password = document.getElementById("password").value ;
            console.log(email, password)
            this.props.onSignIn({
                email,
                password
            })
            return true;
        }



        return (<div className = {classes.Main}>
                <h1>Log In</h1>
                <form className = {classes.Form} onSubmit = {
                    (e)=>{
                        submitForm();
                        return true;
                    }
                }>
                
                    <input value = {this.state.email} type="email" onChange = {
                        (event)=>{

                            this.setState({
                                ...this.state,
                                email: event.target.value,
                                inputsValid:checkValidity(),
                                
                            })
                        }
                    } id="email" name="fname" placeholder = "Email Address"/>
                    <input value = {this.state.password} onChange = {
                        (event)=>{
                            this.setState({
                                ...this.state,
                                password: event.target.value,
                                inputsValid:checkValidity(),
                                
                            })
                        }
                    } type="password" id="password" name="lname" placeholder = "Password"/>
                    {
                        (this.props.errorMessage && this.props.errorMessage.length !== 0)?
                        <text className = {classes.Error}>{this.props.errorMessage}</text>: null

                    }
                    
                   
                   
                    <button disabled = {this.state.inputsValid == false? true: false} className={classes.Submit} > <img  src= {arrow} /></button>
                </form>
                <NavLink exact to="/signup">Don't have an account? Sign up now!</NavLink>
                
        </div>)
    }
}

const mapStateToProps = (state)=>{
    console.log('state from props loading', state)
    return {
        user: state.auth.user,
        inProgress: state.auth.inProgress,
        errorMessage: state.auth.errorMessage
    }
}

const mapDispatchToProps = (dispatch)=>{
    return {
        onSignIn: (data) => {dispatch(authActionCreators.signin(data))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)( LoginScreen);