import React from 'react'
import { ACTION_CLEAR_SEARCH_USERS } from '../store/actions/actionTypes';
import classes from './EditProfileScreen.css'
import { connect } from 'react-redux';
import UserScreen from './UserScreen';
import withMainStore from '../hooks/withMainStore';
import { faUserPlus, faUserMinus, faEdit, faSave } from '@fortawesome/free-solid-svg-icons'
import { withRouter } from 'react-router';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


import * as authActionCreators from '../store/actions/authActions'
import loading from '../assets/images/loading.gif'
import arrow from '../assets/images/arrow.png'
import {NavLink} from 'react-router-dom'
import AuthImage from '../Components/AuthImage';
import userPhoto from '../assets/images/user.png'

class EditProfileScreen extends React.Component{

    constructor(){
        super();
        this.fileInput = React.createRef(null)
        this.state = {
            inputsValid:false,
            firstName: '',
            lastName: '',
           
        }

    }

    


    componentDidMount(){

        //const user = this.props.getUser(this.props.authUser._id);
      /*  this.setState({
            ...this.state,
            firstName: '',
            lastName: '',
            image: null,
            imageFile: null
            
        })*/
    }

    render(){


        let user = null;

        console.log('edit screen auth user', this.props.authUser)

        if (this.props.authUser){
            user = this.props.getUser(this.props.authUser._id);
        }

        console.log('edit screen auth user', this.props.authUser)

        console.log('edit screen local user', user)
        console.log('edit screen state user', this.state.user)

       

        if (! this.state.user ){
            if(user){
                this.setState({
                    ...this.state,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    image: null,
                    imageFile: null,
                    user:user
                    
                })
            }
            return (
                <div className = {classes.ActivityIndicator} >
                    <img src = {loading}/>
                </div>
            )
        }
        const checkFirstName = (firstname)=>{
            if (firstname.length ==0){
                return false;
            }
            const pattern = /^[a-zA-Z]*$/;
            const res =  pattern.test(firstname);
            console.log('res', firstname, res)
            return res;
        }

        const checkLastName = (lastname)=>{
            if (lastname.length ==0){
                return false;
            }
            const pattern = /^[a-zA-Z]*$/;
            const res =  pattern.test(lastname);
            console.log('res', lastname, res)
            return res;
        }



        const checkValidity = ()=>{
   
            const firstName = document.getElementById("firstname").value ;
            const lastName = document.getElementById("lastname").value ;
           
            if(checkLastName(lastName) == false){
                return false;
            }

            if(checkFirstName(firstName) == false){
                return false;
            }
           
            return true;
        }

    

        const submitForm = ()=>{
            console.log("submit!!!!!!!!!!!!!!!")
            const firstName = document.getElementById("firstname").value ;
            const lastName = document.getElementById("lastname").value ;
            this.props.onUpdate({
      
                firstName,
                lastName,
               // imageUri: this.state.image,
                image: this.state.imageFile

              })
            return true;
        }


        console.log('account screen props' ,this.props)

        return (<div className = {classes.Main}>

            
            
            <form className = {classes.Form} onSubmit = {
                (e)=>{
                    e.preventDefault()
                    submitForm();
                    return true;
                }
            }>
            
            <AuthImage onClick = {()=>{
                this.fileInput.current.click()
            }} className = {classes.UserImage} src = {this.state.image?this.state.image:  this.state.user.photo}
            onError= {(param)=>{
                console.log('image error', param.target)
                param.target.src = userPhoto
            }}></AuthImage>

            <input ref = {(a)=>{
                this.fileInput.current = a;
            }}  accept="image/x-png,image/gif,image/jpeg" style={{display: "none"}} type="file" name="file" onChange={(event)=>{
                    console.log('upload', event.target.files)
                    this.setState({
                        ...this.state,
                        image: URL.createObjectURL(event.target.files[0]),
                        imageFile: event.target.files[0],
                        inputsValid:checkValidity()
                    })
                   
            }}/>

                <input value = {this.state.firstName} type="text" onChange = {
                    (event)=>{

                        if (checkFirstName(event.target.value) == false && event.target.value.length != 0){
                            return;
                        }

                        this.setState({
                            ...this.state,
                            firstName: event.target.value,
                            inputsValid:checkValidity(),
                            
                        })
                    }
                } id="firstname" name="fname" placeholder = "First Name"/>      

                <input value = {this.state.lastName} type="text" onChange = {
                    (event)=>{

                        if (checkLastName(event.target.value) == false && event.target.value.length != 0){
                            return;
                        }

                        this.setState({
                            ...this.state,
                            lastName: event.target.value,
                            inputsValid:checkValidity(),
                            
                        })
                    }
                } id="lastname" name="fname" placeholder = "Last Name"/> 


                {
                    (this.props.errorMessage && this.props.errorMessage.length !== 0)?
                    <text className = {classes.Error}>{this.props.errorMessage}</text>: null

                }
                
               
               
                <button disabled = {this.state.inputsValid == false? true: false} className={classes.Submit} > <FontAwesomeIcon width = {50} className = {classes.EditIcon} icon={faSave} /></button>
            </form>
    </div>)
        return <div className = {classes.Main}>
            <UserScreen userId = {this.props.authUser._id} showModal = {this.props.showModal} closeModal = {this.props.closeModal}/>
            <button className = {classes.EditButton} onClick = {()=>{
                       this.props.history.push('/editAccount')
                        
                    }}><FontAwesomeIcon width = {50} className = {classes.EditIcon} icon={faEdit} />
                </button>
        </div>
    }
}



const mapStateToProps = (state)=>{
    console.log('state from props loading', state)
    return {
        authUser: state.auth.user,
        inProgress: state.auth.inProgress,
        errorMessage: state.auth.errorMessage
    }
}

const mapDispatchToProps = (dispatch)=>{
    return {
        onUpdate: (data) => {dispatch(authActionCreators.update(data))}
    }
}


export default  withRouter(connect(mapStateToProps, mapDispatchToProps)(withMainStore(EditProfileScreen)));