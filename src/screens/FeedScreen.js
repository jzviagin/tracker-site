import React, { useEffect, useRef } from 'react'

import withMainStore from '../hooks/withMainStore'

import TrackDetailFeed from '../Components/TrackDetailFeed'
import debounce from "lodash.debounce";
import classes from './FeedScreen.css'
import loading from '../assets/images/loading.gif'
import resolve from 'resolve';

/*const bar  = ()=>{
    return new Promise(async (resolve, reject)=>{
        setTimeout(()=>{
            resolve()
        },2000)
    })
}


console.log('promise before')
const foo  = ()=>{
    return new Promise(async (resolve, reject)=>{
        const x = await bar() 
        for( let i = 0 ; i< 10000; i++){
            console.log('promise running')
        }
        
        resolve(1)
    })
}

foo().then(()=>{
    console.log('promise done')
})

console.log('promise after')*/

const sum = (x)=>{

    return (y) =>{

        if (!y){
            return x
        }
        return sum(x+y)
    }
}

console.log('summmmmmmmmm!', typeof number)
console.log('summmmmmmmmm!', sum(2)(4)(6)())

const Loader = (props)=>{
    const loaderRef = useRef(null)
    const loaded = useRef(false)
   // const [loaded, setLoaded] = useState(false)

    useEffect(()=>{
        window.onscroll = debounce(()=>{
            console.log('scroll', 'onScroll')
                if(loaderRef.current){
                    console.log('scroll',  document.documentElement.clientHeight)
                    console.log('scroll',  loaderRef.current.getBoundingClientRect().top )
                    console.log('scroll',  loaderRef.current.scrollHeight )

                    if (loaderRef.current.getBoundingClientRect().top <= document.documentElement.clientHeight){
                        if (!loaded.current){
                            loaded.current = true;
                            props.onLoad()
                                        
                        }
                    }
                }
          },0)
    }, [])

    console.log('scroll', props)
    return (
        <div ref = {(a)=>{
            loaderRef.current = a;
        }}>
            <img className = {classes.Loader} src = {loading}/>
        </div>
    )

}

class FeedScreen extends React.Component{
    componentWillMount(){
        console.log('componentWillMount!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    }

    constructor(){
       
        super()
        /*window.onscroll = debounce(() => {

            console.log('scroll',  window.innerHeight + document.documentElement.scrollTop)
            console.log('scroll2 ',  document.documentElement.offsetHeight)
            if (
              Math.abs(window.innerHeight + document.documentElement.scrollTop - document.documentElement.offsetHeight) < 50
            ) {
                this.props.fetchFeedPage()
            }
          }, 0);*/

          this.loaderRef = React.createRef(null)

         /* window.onscroll = debounce(()=>{
            console.log('scroll', 'onScroll')
                if(this.loaderRef.current){
                    console.log('scroll',  document.documentElement.clientHeight)
                    console.log('scroll',  this.loaderRef.current.getBoundingClientRect().top )
                    console.log('scroll',  this.loaderRef.current.scrollHeight )

                    if (this.loaderRef.current.getBoundingClientRect().top <= document.documentElement.clientHeight){
                        this.loaderRef.current.loadData()
                    }
                }
          },0)*/
    }

    componentDidMount(){
        this.props.fetchFeedPage()
    }

    render(){

        console.log('feedScreen props', this.props)

        

        const feed = this.props.getFeed();


        
        let trackIds = [];

        const keys = Object.keys(feed);
        for (let i = 0 ; i < keys.length; i++){
            trackIds = [...trackIds, ...feed[i]];

        }
        trackIds.push (0)
        console.log('render feed', feed)

        if (trackIds.length ===0 ){
            return (
                <div className = {classes.ActivityIndicator} >
                    <img src = {loading}/>
                </div>
            )
        }
        return <div className = {classes.Main}>
            <ul className={classes.List}>
                {
                    trackIds.map( (trackId)=>{


                        if (trackId == 0){
                            return <Loader 
                            onLoad = {
                                this.props.fetchFeedPage
                            }
                            refer = {(a)=>{
                                console.log('scroll setting loader ref')
                                this.loaderRef.current = a;
                            }}></Loader>
                        }


                        console.log('track 1', trackId)
                        const track = this.props.getTrack(trackId);
                        console.log('track', track)
                        const user = this.props.getUser(track.userId);
                        console.log('user', user);
                        return <TrackDetailFeed showUser = {true} showModal = {this.props.showModal} closeModal = {this.props.closeModal} user = {user} track= {track}/>

                       
                    })
                }
            </ul>
        </div>
    }
}

export default withMainStore(FeedScreen);

console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!',FeedScreen)
console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!',React.createElement(FeedScreen, null))



