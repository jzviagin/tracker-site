


const message:string = "hello world!";
console.log(message);


async function doParallel<T,S> (
    func: (args: T) => Promise<S>,
    args: T[],
    
    ): Promise<S[]>{
        console.log('parallel')

        const promises = args.map(arg=>func(arg));
        console.log('promises', promises)

        const ret  = Promise.all(args.map(arg=>func(arg)));


        const res = await ret;

        console.log('ret', ret)
        
        return Promise.all(args.map(arg=>func(arg)));
    }
    
const foo = (num:number): Promise<number> =>{
    console.log('foo');
    //return Promise.resolve(num*num)
    return new Promise((resolve, reject)=>{
        console.log('in promise')
        resolve(num*num);
    })
}

const doSomething = async (length = 1)=>{
    const data = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14];
    try{
        const results = await doParallel(foo,data);
        console.log('asdads',results);
    }catch(err){
        console.log('err',err)
    }
    
    

}

doSomething()

export default doSomething;
