import React from 'react';
import {connect} from 'react-redux'
import * as authActionCreators from '../store/actions/authActions'
import loading from '../assets/images/loading.gif'
import classes from './SignupScreen.css'
import arrow from '../assets/images/arrow.png'
import {NavLink} from 'react-router-dom'




class SignupScreen extends React.Component{

    constructor(){
        super();
        this.state = {
            inputsValid:false,
            email: '',
            password: '',
            firstName: '',
            lastName: '',
            username: ''
        }

    }


    componentDidMount(){

    }




    render(){

        console.log('login screen state', this.state)

        if (this.props.inProgress){
            return (
                <div className = {classes.ActivityIndicator} >
                    <img src = {loading}/>
                </div>
            )
        }

        const checkEmail = (email)=>{
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            return pattern.test(email);
        }

        const checkPassword = (password)=>{
            return password.length !== 0;
        }

        const checkFirstName = (firstname)=>{
            if (firstname.length ==0){
                return false;
            }
            const pattern = /^[a-zA-Z]*$/;
            const res =  pattern.test(firstname);
            console.log('res', firstname, res)
            return res;
        }

        const checkLastName = (lastname)=>{
            if (lastname.length ==0){
                return false;
            }
            const pattern = /^[a-zA-Z]*$/;
            const res =  pattern.test(lastname);
            console.log('res', lastname, res)
            return res;
        }

        const checkUsername = (username)=>{
            if (username.length ==0){
                return false;
            }
            const pattern = /^[a-zA-Z0-9]*$/;
            const res =  pattern.test(username);
            console.log('res', username, res)
            return res;
        }

        const checkValidity = ()=>{
            const email = document.getElementById("email").value ;
            const password = document.getElementById("password").value ;
            const username = document.getElementById("username").value ;
            const firstName = document.getElementById("firstname").value ;
            const lastName = document.getElementById("lastname").value ;
            if( checkEmail(email)== false){
                return false;
            }
            if(checkPassword(password) == false){
                return false;
            }

            if(checkLastName(lastName) == false){
                return false;
            }

            if(checkFirstName(firstName) == false){
                return false;
            }
            if(checkUsername(username) == false){
                return false;
            }
            return true;
        }

    

        const submitForm = ()=>{
            console.log("submit!!!!!!!!!!!!!!!")
            const email = document.getElementById("email").value ;
            const username = document.getElementById("username").value ;
            const firstName = document.getElementById("firstname").value ;
            const lastName = document.getElementById("lastname").value ;
            const password = document.getElementById("password").value ;
            console.log(email, password)
            this.props.onSignUp({
                email,
                password,
                username,
                firstName,
                lastName
              })
            return true;
        }



        return (<div className = {classes.Main}>
                <h1>Sign Up</h1>
                <form className = {classes.Form} onSubmit = {
                    (e)=>{
                        submitForm();
                        return true;
                    }
                }>
                
                    <input value = {this.state.email} type="email" onChange = {
                        (event)=>{

                            this.setState({
                                ...this.state,
                                email: event.target.value,
                                inputsValid:checkValidity(),
                                
                            })
                        }
                    } id="email" name="fname" placeholder = "Email Address"/>

                    <input value = {this.state.username} type="text" onChange = {
                        (event)=>{

                            if (checkUsername(event.target.value) == false && event.target.value.length != 0){
                                return;
                            }

                            this.setState({
                                ...this.state,
                                username: event.target.value,
                                inputsValid:checkValidity(),
                                
                            })
                        }
                    } id="username" name="fname" placeholder = "User Name"/>

                    <input value = {this.state.firstName} type="text" onChange = {
                        (event)=>{

                            if (checkFirstName(event.target.value) == false && event.target.value.length != 0){
                                return;
                            }

                            this.setState({
                                ...this.state,
                                firstName: event.target.value,
                                inputsValid:checkValidity(),
                                
                            })
                        }
                    } id="firstname" name="fname" placeholder = "First Name"/>      

                    <input value = {this.state.lastName} type="text" onChange = {
                        (event)=>{

                            if (checkLastName(event.target.value) == false && event.target.value.length != 0){
                                return;
                            }

                            this.setState({
                                ...this.state,
                                lastName: event.target.value,
                                inputsValid:checkValidity(),
                                
                            })
                        }
                    } id="lastname" name="fname" placeholder = "Last Name"/> 

                    <input value = {this.state.password} onChange = {
                        (event)=>{
                            this.setState({
                                ...this.state,
                                password: event.target.value,
                                inputsValid:checkValidity(),
                                
                            })
                        }
                    } type="password" id="password" name="lname" placeholder = "Password"/>
                    {
                        (this.props.errorMessage && this.props.errorMessage.length !== 0)?
                        <text className = {classes.Error}>{this.props.errorMessage}</text>: null

                    }
                    
                   
                   
                    <button disabled = {this.state.inputsValid == false? true: false} className={classes.Submit} > <img  src= {arrow} /></button>
                </form>
                <NavLink  exact to="/">Already have an account? Sign In!</NavLink>
        </div>)
    }
}

const mapStateToProps = (state)=>{
    console.log('state from props loading', state)
    return {
        user: state.auth.user,
        inProgress: state.auth.inProgress,
        errorMessage: state.auth.errorMessage
    }
}

const mapDispatchToProps = (dispatch)=>{
    return {
        onSignUp: (data) => {dispatch(authActionCreators.signup(data))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)( SignupScreen);