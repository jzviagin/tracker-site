import React from 'react';
import {connect} from 'react-redux'
import * as authActionCreators from '../store/actions/authActions'


class MainScreen extends React.Component{


    componentDidMount(){
        this.props.onSignIn({email: 'jzviagin@gmail.com', password:'12345'})
    }
    render(){
        return <div>
            <text>main</text>
            <h1>{this.props.user?this.props.user.username:'undef'}</h1>
        </div>
    }
}

const mapStateToProps = (state)=>{
    return {
        user: state.auth.user
    }
}

const mapDispatchToProps = (dispatch)=>{
    return {
        onSignIn: (data) => {dispatch(authActionCreators.signin(data))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)( MainScreen);