import * as actionTypes from './actionTypes'

import trackerApi from '../../api/tracker'


import * as mainActions from './mainActions'



const clearErrorAction = () => ()=>{
    return {
        type: actionTypes.ACTION_CLEAR_ERROR    
     };
}


const loginAction = (token, user)=>{
    return {
        type: actionTypes.ACTION_LOGIN,
        token: token,
        user: user
    }
}



const updateAction = (user)=>{
    return {
        type: actionTypes.ACTION_UPDATE,
        user: user }
}


const setErrorMessageAction = (errorMessage)=>{
    return {
        type: actionTypes.ACTION_SET_ERROR_MESSAGE,
        payload: errorMessage
    }
}


const actionStartedAction = ()=>{
    return {
        type: actionTypes.ACTION_STARTED
    }
}

const signOutAction = ()=>{
    return {
        type: actionTypes.ACTION_SIGN_OUT
    }
}




export const tryLocalSignIn = ()=>{

    return async (dispatch, getState)=>{
        try{
            dispatch(actionStartedAction());
            console.log("try");
            const token = await localStorage.getItem('token');
            console.log("token", token);
            if (token){
                const response = await trackerApi.get('/login');
                console.log(response.data);
                dispatch(loginAction(response.data.token, response.data.user)) ;
                return;
            }else{
                dispatch(setErrorMessageAction(''));
            }
            
        }catch(error){
            console.log("error", error);
            dispatch(setErrorMessageAction(error.response.data));
          
        }
    }
} 


export const signup = (data)=>{
    return async (dispatch, getState)=>{
        console.log(data);

        dispatch(actionStartedAction());

        try{

            console.log("signup1");
            const response = await trackerApi.post('/signup', {
                email: data.email,
                password: data.password,
                username: data.username,
                firstName: data.firstName,
                lastName: data.lastName
              });
              console.log("signup2");
              console.log(response);

              await localStorage.setItem('token', response.data.token);
              const token = await localStorage.getItem('token');
              console.log("token2", token);
              dispatch(loginAction(response.data.token, response.data.user));            
        }catch(error){
            console.log("signup error", error.message);
            console.log(error);
            dispatch(setErrorMessageAction(error.response.data));
        }
    }

} 


export const update = (data, callback)=>{
    return async (dispatch, getState)=>{
        console.log('update',data);

        dispatch(actionStartedAction());
    
        try{
    
            const state = getState();
            console.log('update state', state);
            const formData = new FormData();
    
            console.log('image uri', data.imageUri, data);
            if (data.imageUri){
                console.log('appending image')
                formData.append('image', {
                uri:  data.imageUri ,
                name: `image`,
                type: 'image/jpeg', 
              });
            }
            if (data.image){
                //data.image.name = "image"
                //data.image.type = "image/jpeg"
                console.log('appending image', data.image)
                formData.append('image', data.image,'image');
            }
    
            formData.append('firstName', data.firstName);
            formData.append('lastName', data.lastName);
           
            const headers = {
              'Content-Type': 'multipart/form-data',
              'Access-Control-Allow-Origin': '*',
              'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS'
            }
            console.log('update before post');

           /* const response = await fetch('http://ec2-18-217-89-47.us-east-2.compute.amazonaws.com/updateUser', {
                headers,
                method: 'post',
                body: formData
            })*/
            const response = await trackerApi.post('/updateUser', formData , headers);
            console.log('update after post');
              console.log('update',response);

              dispatch(mainActions.getUser(state.auth.user._id))
    
    
           // dispatch(updateAction(response.data.user));
           
           // callback();
            
        }catch(error){
            console.log("update error", error);
            console.log(error);
            //dispatch(setErrorMessageAction(error.response.data));
        }
            
    }
}
  
  



export const signin = (data) => {

        return async(dispatch, getState)=>{
           //make api request 
            //if we sign up, modify the state

            //show message on failure

            dispatch(actionStartedAction());

            console.log(data);

            try{
                console.log('logging in');
                const response = await trackerApi.post('/login', {
                    email: data.email,
                    password: data.password
                });
                console.log('res!!!!!!!', response.data);
                await localStorage.setItem('token', response.data.token);
                dispatch(loginAction(response.data.token, response.data.user));
                
            }catch(error){
                //console.log(error.response.data);
                console.log('log in error', error);
                dispatch(setErrorMessageAction(error.response.data))
            }

    }
}



export const signout = ()=>{

    return async (dispatch, getState)=>{
        try{
            console.log("try");
            await localStorage.setItem('token', '');
            dispatch(signOutAction())
        }catch(error){
            console.log("error", error);
          
        }
    }

} 




