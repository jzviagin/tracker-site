export const ACTION_TEST = 'test';
export const ACTION_LOGIN = 'login';
export const ACTION_SIGN_OUT = 'signout';
export const ACTION_UPDATE = 'update';
export const ACTION_SET_ERROR_MESSAGE = 'set_error_message';
export const ACTION_CLEAR_ERROR =  'clear_error';
export const ACTION_STARTED =  'action_started';


export const ACTION_CLEAR_SEARCH_USERS =  'clear_search_users';
export const ACTION_SET_SEARCH_TERM =  'set_search_term';
export const ACTION_SET_DATA =  'set_data';
export const ACTION_CLEAR_FEED = 'clear_feed';
export const ACTION_CLEAR_USER_TRACKS = 'clear_user_tracks';
