import * as actionTypes from './actionTypes'

import trackerApi from '../../api/tracker'


const inProgressApiCalls = {}

const apiCall = async (api, params, dispatch) => {
    const callKey = api + '_' + JSON.stringify(params);
    try{

       
        if (inProgressApiCalls[callKey]){
            return;
        }
        inProgressApiCalls[callKey] = true;
        console.log('params', params)
        const response = await trackerApi.post('/'+ api, params);
        inProgressApiCalls[callKey] = false;
        console.log(response.data);
        dispatch ({
            type: actionTypes.ACTION_SET_DATA,
            data: response.data
        });
             
            
    }catch(error){
        inProgressApiCalls[callKey] = false;
        console.log(error);
            
    }
}




export const getUser = (userId)=>{

    return (dispatch, getState)=>{
        apiCall('getUser' ,{userId: userId}, dispatch);
    }

   
        
}




export const getTrack = (trackId)=>{

    return (dispatch, getState)=>{
        apiCall('getTrack' ,{trackId: trackId}, dispatch);
    }
    
  

   
        
}

export const getComment =  (commentId)=>{


    return (dispatch, getState)=>{
        console.log('apiCall', commentId)
        apiCall('getComment' ,{commentId: commentId}, dispatch);
    }
  

   
        
}

export const getFollowing = (userId)=>{

    return (dispatch, getState)=>{
        apiCall('getFollowing' ,{userId: userId}, dispatch);
    }
  
  

   
        
}


export const createTrack = (locations, description)=>{

    return (dispatch, getState)=>{
        apiCall('createTrack', {
            description,
            locations
          },dispatch );
    }
   
}



export const getLikes = (trackId)=>{
    return (dispatch, getState)=>{
        apiCall('getLikes' ,{trackId: trackId}, dispatch);
    }
}

export const getComments =  (trackId)=>{
    return (dispatch, getState)=>{
        apiCall('getComments' ,{trackId: trackId}, dispatch);
    }
  

   
        
}



export const like = (trackId)=>{

    return (dispatch, getState)=>{
        apiCall('like' ,{trackId: trackId}, dispatch);
    } 
}


export const addComment =  (trackId, text)=>{
    return (dispatch, getState)=>{
        apiCall('addComment' ,{trackId: trackId, text: text}, dispatch);
    }    
}


export const unlike = (trackId)=>{
    
    return (dispatch, getState)=>{
        apiCall('unlike' ,{trackId: trackId}, dispatch);
    }    
}


export const follow =  (userId)=>{
    return (dispatch, getState)=>{
        apiCall('follow' ,{userId: userId}, dispatch);
    }  
}

export const unfollow = (userId)=>{

    return (dispatch, getState)=>{
        apiCall('unfollow' ,{userId: userId}, dispatch);
    }  
}

export const getFollowers =  (userId)=>{

    return (dispatch, getState)=>{
        apiCall('getFollowers' ,{userId: userId}, dispatch);
    }  
     
}







export const searchUsers =  (username)=>{
    return (dispatch, getState)=>{
        dispatch ({
            type: actionTypes.ACTION_SET_SEARCH_TERM,
            value: username
        });

        if (username.trim().length > 0){
            apiCall('findUsers' ,{username: username}, dispatch);
        }else{
            dispatch ({
                type: actionTypes.ACTION_CLEAR_SEARCH_USERS
               
            });
        }
    } 
}





export const clearUserTracks = ()=>{
    return (dispatch, getState)=>{
        dispatch ({
            type: actionTypes.ACTION_CLEAR_USER_TRACKS
        });
    }  
   
}

export const clearFeed = ()=>{
    return (dispatch, getState)=>{
        dispatch ({
            type: actionTypes.ACTION_CLEAR_FEED
        });
    }  
}

export const fetchUserTracksPage = (userId)=>{

    return (dispatch, getState)=>{
        const state = getState();
        console.log('get user tracks page')
        const keys = Object.keys(state.main['userTracks']);
        console.log('get user tracks page ', keys)
        apiCall('userTracks' ,{page: keys.length, userId}, dispatch);
    }  

   


}

export const fetchFeedPage =  ()=>{


    return (dispatch, getState)=>{
        const state = getState();
        console.log('get feed page state', state)
        console.log('get feed page get state', getState)
        console.log('get feed page')
        const keys = Object.keys(state.main['feed']);
        console.log('feed keys ', keys)
        apiCall('feed' ,{page: keys.length}, dispatch);
    }  

}








