


import * as actionTypes from '../actions/actionTypes'


const initialState = { feed: {}, userTracks: {}, search: {}, searchTerm: '', users:{}, followers:{}, following: {}, tracks:{}, likes: {}, comments:{}, commentsData: {}}


const mainReducer = (state = initialState, action) => {
    switch(action.type){

        case actionTypes.ACTION_CLEAR_SEARCH_USERS:
            return {...state, search: {...state.search, users:[]}}

        case actionTypes.ACTION_SET_SEARCH_TERM:
            return {...state, searchTerm: action.value}


        case actionTypes.ACTION_SET_DATA:

            if (action.data.feed){
                const feedKeys = Object.keys(action.data.feed);
                const stateFeedKeys = Object.keys(state.feed);
                for (let i = 0 ; i < feedKeys.length; i++){
                    const key = feedKeys[i];
                    if (key > stateFeedKeys.length){
                        console.log('feed update ignored')
                        return state;
                    }
                }
            }

            if (action.data.userTracks){
                const userTracksKeys = Object.keys(action.data.userTracks);
                const stateuserTracksKeys = Object.keys(state.userTracks);
                for (let i = 0 ; i < userTracksKeys.length; i++){
                    const key = userTracksKeys[i];
                    if (key > stateuserTracksKeys.length){
                        console.log('feed update ignored')
                        return state;
                    }
                }
            }
            if (action.data.search && state.searchTerm.trim().length == 0){
                return state;
            }
            //console.log('!!!!!!!!!!received data', JSON.stringify(action.data));
            const newState = {...state};
            const keys = Object.keys(action.data);
            for (let i = 0 ; i < keys.length; i++){
                const entityName = keys[i]
                //console.log('entity name', entityName)

                const entity = {...state[entityName]};
                const ids = Object.keys(action.data[entityName]);
                for (let j = 0 ; j < ids.length; j++){
                    const id = ids[j];
                    entity[id] = action.data[entityName][id]
                }
                newState[entityName] = {...entity}
            }
            //console.log('new state', newState);
            return newState;
        


        case actionTypes.ACTION_CLEAR_FEED:
            return {...state, feed: {}}
        case actionTypes.ACTION_CLEAR_USER_TRACKS:
            return {...state, userTracks: {}}
        default:
            return state;
    }
}

export default mainReducer;
