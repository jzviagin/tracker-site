

import * as actionTypes from '../actions/actionTypes'


const initialState =  {errorMessage: '',  inProgress: false}


const authReducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.ACTION_TEST:
            console.log ('test dispatched')
            return state;
        case actionTypes.ACTION_LOGIN:
            return {...state, token: action.token,  user:action.user, errorMessage: '',  inProgress: false};

        case actionTypes.ACTION_UPDATE:
            return {...state, user:action.user, errorMessage: '',  inProgress: false};
    


        case actionTypes.ACTION_SET_ERROR_MESSAGE:
            return {...state, errorMessage: action.payload,  inProgress: false};

        case actionTypes.ACTION_CLEAR_ERROR:
            return {...state, errorMessage: ''};
        case actionTypes.ACTION_STARTED:
            return {...state, inProgress: true};
        case actionTypes.ACTION_SIGN_OUT: 
            return {...state, token: null,  user:null, errorMessage: '',  inProgress: false};
        default:
            return state;
    }
}

export default authReducer;