import React, { useEffect , useContext, useState, useRef} from 'react';
import AuthImage from '../Components/AuthImage'
import UserDetailsSmall from '../Components/UserDetailsSmall'
import FeedComments from '../Components/FeedComments'
import classes from './TrackDetailFeed.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHeart as heartSolid, faCoffee, faCaretLeft, faCaretRight, faSearchPlus, faSearchMinus, faChartBar, faChartLine, faAngleDoubleRight, faAngleDoubleLeft } from '@fortawesome/free-solid-svg-icons'
import { faHeart as heartRegular } from '@fortawesome/free-regular-svg-icons'
import UserList from './UserList';
import  withMainStore from '../hooks/withMainStore'
import {connect} from 'react-redux'





const TrackDetailFeed = (props) => {

    const obj = useRef(new Date());

    const [st,setSt] = useState({})

    useEffect(()=>{
        console.log('test render2', obj.current)
        setSt(new Date())
    },[obj])
    console.log('test render1')

    console.log('TrackDetailFeed props', props)

    const likes = props.getLikes(props.track._id)
    let liked = false;
    for (let i = 0 ; i < likes.length ; i++){
        if (likes [i] == props.authUser._id){
            liked = true;
        }
    }
    const likesUsers = likes.map( (id)=>{
        return props.getUser(id);
    });
    const commentsIds  = props.getComments(props.track._id)

   


    let path = "path=color:0x0000ff";
    for (let i = 0 ; i < props.track.locations.length ; i++){
        path = path + "|" + props.track.locations[i].coords.latitude + "," + props.track.locations[i].coords.longitude  
    }

    const mapUri = "https://maps.googleapis.com/maps/api/staticmap?size=1200x1200&" + path + "&key=AIzaSyB28jN9Ez3EaS8uRUPkMMj-OB_AtGzlRss"
    console.log("path", mapUri);
    return <div className={classes.Main}>


        {props.showUser?<UserDetailsSmall user = {props.user}></UserDetailsSmall>:null}
        <p className = {classes.Description}>{props.track.description}</p>
        
        
        <img className={classes.Map}
            src={ mapUri  }
            />
        <div className = {classes.Links}>
            <button className = {classes.LikeButton} onClick = {()=>{
                if (liked){
                    props.unlike(props.track._id)
                }else{
                    props.like(props.track._id)
                }
                
            }}><FontAwesomeIcon width = {50} className = {classes.LikeIcon} icon={liked == false  ?heartRegular: heartSolid} /></button>
                {likesUsers.length != 0 ? <a to="" onClick={()=>{props.showModal(<UserList title = { "Likes"} users = {likesUsers}/>)}} className= {classes.NavLink} >{likesUsers.length == 1? "1 Like" : likesUsers.length + " Likes"}</a>:null}
                {commentsIds.length != 0 ? <a to="" onClick={()=>{props.showModal(<FeedComments track = {props.track}/>)}} className= {classes.NavLink} >{commentsIds.length == 1? "1 Comment" : commentsIds.length + " Comments"}</a>:null}
        </div>
        <FeedComments limitComments = {3} track = {props.track}/>


    </div>
};


const mapStateToProps = (state)=>{
    console.log('mapStateToProps', this.props)
    return {
        authUser: state.auth.user,
        inProgress: state.auth.inProgress,
        errorMessage: state.auth.errorMessage
    }
}

const mapDispatchToProps = (dispatch)=>{
    return {
        
    }
}


export default  connect(mapStateToProps, mapDispatchToProps)(withMainStore(TrackDetailFeed));