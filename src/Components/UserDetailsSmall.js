import React from 'react'
import {NavLink} from 'react-router-dom'
import classes from './UserDetailsSmall.css'
import logo from '../assets/images/logo.png'
import AuthImage from './AuthImage'
import user from '../assets/images/user.png'
class UserDetailsSmall extends React.Component{
    render(){
        return (<div className={classes.UserDetails}>
                <div className={classes.Image}>
                    <AuthImage  className ={classes.Img} src = {this.props.user.photo} onError= {(param)=>{
                        console.log('image error', param.target)
                        param.target.src = user
                    }}></AuthImage>
                </div>
                <NavLink className= {classes.NavLink} exact to= {"/user/" + this.props.user._id} >{this.props.user.username}</NavLink>
            </div>)
    }
}

export default UserDetailsSmall;