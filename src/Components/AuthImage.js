import React, { useContext, useEffect, useState} from 'react';

import {connect} from 'react-redux'


 
// More info on all the options is below in the API Reference... just some common use cases shown here





const AuthImage = (props) => {

    const [url, setUrl] = useState(null);



    useEffect(()=>{
        const options = {
            headers: {
                Authorization: 'Bearer ' + props.token
            }
          };

          console.log('options', props.token)

          console.log('fetching', props.src); 
          
          fetch(props.src, options)
          .then(res => {
            console.log('res', res)
            return res.blob()
        })
          .then(blob => {
            console.log('blob', blob)
            const localUrl = URL.createObjectURL(blob)
            console.log('local url', localUrl);

            setUrl(localUrl)
          });
    },[props.src]);


   

   

    
 
    
     console.log('image url', url)


    return (
        //<img {...newProps}/>
        //<img scr = 'https://cdn.pixabay.com/photo/2015/09/09/16/05/forest-931706_960_720.jpg' width ={400} height={400}/>
        <img {...props} src={url} alt="" />
        
    )

    
};


const mapStateToProps = (state)=>{
    console.log('mapStateToProps', state)
    return {
        token: state.auth.token
    }

}

const mapDispatchToProps = (dispatch)=>{
    return {

    }
}






export default connect(mapStateToProps, mapDispatchToProps)(AuthImage);