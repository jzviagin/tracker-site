import React from 'react'
import {NavLink} from 'react-router-dom'
import classes from './NavBar.css'
import logo from '../assets/images/logo.png'
class NavBar extends React.Component{
    render(){
        return <header className={classes.Toolbar}>
            <div className={classes.Logo}>
                <img src = {logo}></img>
            </div>
            
            <nav className={classes.Nav}>

                <ul className={classes.NavigationItems}>

                    
                    <li className = {classes.NavigationItem}>
                        <NavLink exact to="/feed">Feed</NavLink>
                    </li>
                    <li className = {classes.NavigationItem}>
                        <NavLink exact to="/search">Search</NavLink>
                    </li>
                    <li className = {classes.NavigationItem}>
                    
                        <NavLink exact to="/account">Account</NavLink>
                    </li>
                
                    

                </ul>
            </nav>
            <NavLink   onClick= {()=>{this.props.onSignOut()}} className = {classes.Signout} exact to="/">Sign Out</NavLink>
            
        </header>
    }
}

export default NavBar;