import React from 'react'

import withMainStore from '../hooks/withMainStore'

import TrackDetailFeed from './TrackDetailFeed'
import debounce from "lodash.debounce";
import classes from './UserTracks.css'
import loading from '../assets/images/loading.gif'
import {useEffect}  from 'react'



const Loader = withMainStore((props)=>{
    
    useEffect(()=>{
        console.log('loader render !!!!');
        props.fetchUserTracksPage(props.userId)
    }, [])
    
    return (
        <div>
            <img src = {loading}/>
        </div>
    )
})


class UserTracks extends React.Component{

    constructor(){
        super()
        window.onscroll = debounce(() => {
            if (
              window.innerHeight + document.documentElement.scrollTop
              === document.documentElement.offsetHeight
            ) {
                this.props.fetchUserTracksPage(this.props.userId)
            }
          }, 3000);
    }

    componentDidMount(){
        //this.props.fetchFeedPage()
        this.props.clearUserTracks();
    }

    render(){

        const feed = this.props.getUserTracks();


        
        let trackIds = [];

        const keys = Object.keys(feed);
        for (let i = 0 ; i < keys.length; i++){
            trackIds = [...trackIds, ...feed[i]];
            if (feed[i].length === 0){
                //trackIds = [...trackIds, 0];
            }else{
               
            }
            

        }

        //console.log('render feed', feed)

        if (keys.length === 0 || feed[keys[keys.length - 1]].length !==0){
            trackIds = [...trackIds, 0];
        }

        /*if (trackIds.length ===0 ){
            return (
                <div className = {classes.ActivityIndicator} >
                    <img src = {loading}/>
                </div>
            )
        }*/

        console.log('render ids', trackIds)
        return <div className = {classes.Main}>
            <h1>{this.props.title}</h1>
            <ul >
                {
                    trackIds.map( (trackId)=>{


                        if (trackId !== 0){
                            const track = this.props.getTrack(trackId);
                            console.log('track', track)
                            const user = this.props.getUser(track.userId);
                            console.log('user', user);
                            return <TrackDetailFeed showModal = {this.props.showModal} closeModal = {this.props.closeModal} user = {user} track= {track}/>
                        }else{
                            return (
                                <Loader {...this.props}/>
                            )
                        }


                       
                    })
                }
            </ul>
        </div>
    }
}

export default withMainStore(UserTracks);