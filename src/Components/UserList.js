import React from 'react'
import {NavLink} from 'react-router-dom'
import classes from './UserList.css'
import logo from '../assets/images/logo.png'
import AuthImage from './AuthImage'
import UserDetailsSmall from './UserDetailsSmall'
class UserList extends React.Component{
    
    render(){
        console.log('user list', this.props.users);
        return (<div className={classes.Main}>
            <h1>{this.props.title}</h1>
                <ul>
                    {
                        this.props.users.map((user)=>{
                            return ( <li >
                                <UserDetailsSmall user={user}></UserDetailsSmall>
                            </li> )
                           
                        })
                    }
                </ul>
            </div>)
    }
}

export default UserList;