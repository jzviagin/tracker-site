import React, { useEffect , useContext, useState, useRef} from 'react';
import AuthImage from './AuthImage'
import UserDetailsSmall from './UserDetailsSmall'
import Comment from './Comment'
import withMainStore from '../hooks/withMainStore'
import classes from './FeedComments.css'
import arrow from '../assets/images/arrow.png'

const FeedComments = (props) => {

    const [commentText, setCommentText] = useState('')

    let commentsIds  = props.getComments(props.track._id)
    if (props.limitComments && commentsIds.length > props.limitComments){
        commentsIds = commentsIds.slice(-props.limitComments )
    }

    console.log('commentsIds', commentsIds)

    const textArea = useRef(null)
    const listRef = useRef(null);


    const scrollToBottom = ()=>{
        if (listRef.current ){
            
            listRef.current.scrollTop = listRef.current.scrollHeight;
        }
    }

    useEffect(()=>{
        scrollToBottom();
    },[commentsIds.length])

    return (<div className = {classes.Main}>
        <ul ref = {(a)=>{listRef.current = a}}>
        {commentsIds.map((id)=>{
            console.log('comment and track id' ,props.track._id, id)
            return (
            <li>
                <Comment id = {id}></Comment>
            </li>)
           
        })}
        </ul>
        <div className = {classes.Input}>
            <textarea ref = {(a)=>{textArea.current = a}} rows = {1} className = {classes.TextArea} value = {commentText} type="text" onChange = {
                            (event)=>{
                                setCommentText(event.target.value)
                       
                                setTimeout (()=>{
                                    let  text = textArea.current;
                                  
                                    text.style.height = 'auto';
                                    text.style.height = text.scrollHeight+'px';
                                },100)
                                
                                    /* 0-timeout to get the already changed text */
                                  
                                        //window.setTimeout(resize, 0);
                                  
                                
                            }
                        } id="text" name="fname" placeholder = "Write a comment..."/>
        <button disabled = {commentText.length == 0? true: false} className={classes.Submit} onClick = {()=>{
            props.addComment(props.track._id, commentText);
            setCommentText('')
        }}> <img  src= {arrow} /></button>
        </div>
        

    </div>)
};



export default withMainStore(FeedComments);