import React, { useEffect , useContext, useState, ReactElements} from 'react';
import AuthImage from './AuthImage'
import UserDetailsSmall from './UserDetailsSmall'
import withMainStore from '../hooks/withMainStore'
import classes from './Comment.css'
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';


const Comment = (props) => {

    const comment  = props.getComment(props.id)
    const user = props.getUser(comment.userId)

    useEffect(()=>{
        console.log('testRender2', props.id)
        return ()=>{
            console.log('testRender1', props.id)
        }
        
    })
    console.log('testRender', props.id, ReactElements)
  
    return (
        <div className={classes.Comment}>
                <div className={classes.Image}>
                    <AuthImage  className ={classes.Img} src = {user.photo}></AuthImage>
                </div>
                <div className = {classes.Vertical}>
                    <NavLink className= {classes.NavLink} exact to= {"/user/" + user._id} >{user.username}</NavLink>
                    <text className = {classes.CommentText}>{comment.text}</text>
                    
                </div>
           
  
            </div>
    )

    return (<div>
        {JSON.stringify(comment)}
        {JSON.stringify(user)}
        <div>
            <div>
            <div className={classes.Image}>
                    <AuthImage  className ={classes.Img} src = {user.photo}></AuthImage>
                </div>
                <div>

                </div>
            </div>
        </div>
    </div>)
};





export default withMainStore(Comment);