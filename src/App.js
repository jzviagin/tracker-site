import React, { Component } from 'react';
import logo from './logo.svg';
import classes from './App.css';
import {BrowserRouter, Switch, Router, Route} from 'react-router-dom'
import NavBar from './Components/NavBar'
import FeedScreen from './screens/FeedScreen'
import AccountScreen from './screens/AccountScreen'
import SearchScreen from './screens/SearchScreen'
import LoginScreen from './screens/LoginScreen'
import SignupScreen from './screens/SignupScreen'
import EditProfileScreen from './screens/EditProfileScreen'
import UserScreen from './screens/UserScreen'
import MainScreen from './screens/MainScreen'
import * as authActionCreators from './store/actions/authActions'
import {connect} from 'react-redux'
import Modal from './Components/Modal'




class App extends Component {

  constructor(){
    super()
    this.state = {
      modalComponent: null,

    }
  }

  componentDidMount(){
    this.props.onSignIn()
  }
  render() {

    if (this.props.inProgress){
        
    }

    console.log('app props', this.props);

    if (!this.props.user){
      return (
        <BrowserRouter>
          <Switch>
              <Route path ="/signup" exact component = {SignupScreen}/>
              <Route path ="/"  component = {LoginScreen}/>
          </Switch>
        </BrowserRouter>

      )
    }

    const signOutComponent =  <div className = {classes.Modal}>
                                <h1>Sign out?</h1>
                                <div>
                                  <button onClick = {()=>{this.props.onSignOut()}}>Yes</button>
                                  <button onClick = {()=>{
                              this.setState({... this.state, modalComponent: null})
                            }}>No</button>
                                </div>
                              </div>

    const showModal = (comp)=>{
      document.body.style.overflow = 'hidden';
      this.setState({... this.state, modalComponent: comp});
    }

    const closeModal = ()=>{
      document.body.style.overflow = 'scroll';
      this.setState({... this.state, modalComponent: null});
    }
   

    return (

          <div className={classes.App}>

            <BrowserRouter>
            <Modal show = {this.state.modalComponent}
              backdropClickedHandler = {()=>{
                document.body.style.overflow = 'scroll';
                this.setState({... this.state, modalComponent: null})
              }}>
                {
                  this.state.modalComponent
                }
              
            </Modal>
            <NavBar onSignOut = {()=>{this.setState({... this.state, modalComponent: signOutComponent})}}></NavBar>
              <Switch>
                <Route path ="/user/:id" render = {(props)=>{
                  console.log('user screen props', props)
                  
                  return <UserScreen userId = {props.match.params.id} showModal = {showModal} closeModal = {closeModal}/>
                }}/>
                <Route path ="/feed" exact render = {(props)=>{
                  
                  return <FeedScreen  showModal = {showModal} closeModal = {closeModal}/>
                }}/>
                <Route path ="/search" component = {SearchScreen}/>
                <Route path ="/editAccount" render = {(props)=>{
                  
                  return <EditProfileScreen  showModal = {showModal} closeModal = {closeModal}/>
                }}/>
                <Route path ="/account" render = {(props)=>{
                  
                  return <AccountScreen  showModal = {showModal} closeModal = {closeModal}/>
                }}/>
                <Route path ="/"  render = {(props)=>{
                  
                  return <FeedScreen  showModal = {showModal} closeModal = {closeModal}/>
                }}/>
              </Switch>
            </BrowserRouter>
          </div>
      
    );
  }
}

//export default App;

const mapStateToProps = (state)=>{
  return {
      user: state.auth.user,
      inProgress: state.auth.inProgress

  }
}

const mapDispatchToProps = (dispatch)=>{
  return {
      onSignIn: (data) => {dispatch(authActionCreators.tryLocalSignIn())},
      onSignOut: ()=> {dispatch(authActionCreators.signout())} 
  }
}

export default connect(mapStateToProps, mapDispatchToProps)( App);
