
import React from 'react';
import {connect} from 'react-redux'
import * as mainActionCreators from '../store/actions/mainActions'


const withMainStore = (Child)=>{
    const MainComp = (props)=>{


        const funcs = {

            getUser :  (userId)=>{
                if (props['users'][userId]){
                    return props['users'][userId]
                }
                props.dispatch(mainActionCreators.getUser(userId));
                return null;
            
    
            
                    
            },
    
            fetchUser : (userId)=>{
    
                props.dispatch(mainActionCreators.getUser(userId));
                return null;     
            },
    
    
            getTrack : (trackId)=>{
                console.log('track 3', trackId)
                console.log('track 3', props['tracks'])
                if (props['tracks'][trackId]){
                    return props['tracks'][trackId]
                }
                props.dispatch(mainActionCreators.getTrack(trackId))      
                return null;  
            },
    
            getComment :  (commentId)=>{
                if (props['commentsData'][commentId]){
                    return props['commentsData'][commentId]
                }
                console.log('withmainstore get comment', commentId)
                props.dispatch(mainActionCreators.getComment(commentId))  
                return null;   
            },
    
            getFollowing :  (userId)=>{
                
                if (props['following'][userId]){
                    return props['following'][userId]
                }
                props.dispatch(mainActionCreators.getFollowing(userId)) 
                return [];
            
    
            
                    
            },
    
    
    
            createTrack : (dispatch, state) => (locations, description)=>{
                props.dispatch(mainActionCreators.createTrack(description, locations)) 
            },
    
    
    
            getLikes : (trackId)=>{
    
    
                if (props['likes'][trackId]){
                    return props['likes'][trackId]
                }
                props.dispatch(mainActionCreators.getLikes(trackId))
                return [];
            
    
            
                    
            },
    
            getComments :  (trackId)=>{
                
                if (props['comments'][trackId]){
                    return props['comments'][trackId];
                }
                console.log('getComments no comments for track', trackId)
                props.dispatch(mainActionCreators.getComments(trackId))
                return [];
            
    
            
                    
            },
    
    
    
            like :  (trackId)=>{
                props.dispatch(mainActionCreators.like(trackId))
            },
    
    
            addComment : (trackId, text)=>{
                props.dispatch(mainActionCreators.addComment(trackId, text))   
            },
    
    
            unlike :  (trackId)=>{
                props.dispatch(mainActionCreators.unlike(trackId))    
            },
    
    
            follow : (userId)=>{
                

                props.dispatch(mainActionCreators.follow(userId))   
            
            
    
            
                    
            },
    
            unfollow :  (userId)=>{
                props.dispatch(mainActionCreators.unfollow(userId))   
            },
    
            getFollowers : (userId)=>{
    
                
                if (props['followers'][userId]){
                    return props['followers'][userId]
                }
                props.dispatch(mainActionCreators.getFollowers(userId))
                return [];
            
    
            
                    
            },
    
    
            getUsers : ()=>{
    
                
                if (props['search']['users']){
                    return props['search']['users']
                }
                return [];
            },
    
    
    
    
            searchUsers : (username)=>{
                props.dispatch(mainActionCreators.searchUsers(username))
                return [];
            },
    
    
            getFeed : ()=>{
                console.log('get feed ', props)
                return props['feed'];
            },
    
            getUserTracks : ()=>{
                return props['userTracks'];
            },
    
            clearUserTracks :  ()=>{
       
                props.dispatch(mainActionCreators.clearUserTracks())
            },
    
            clearFeed : ()=>{
                props.dispatch(mainActionCreators.clearFeed())
            },
    
            fetchUserTracksPage :  (userId)=>{
                props.dispatch(mainActionCreators.fetchUserTracksPage(userId))
            },
    
            fetchFeedPage : ()=>{
                props.dispatch(mainActionCreators.fetchFeedPage())
            
    
    
            }

        }


        const newProps = {...props, ...funcs}

      

        return <Child {...newProps}></Child>
    }

    const mapStateToProps = (state)=>{
        return { 
            userTracks: state.main.userTracks, 
            feed: state.main.feed, 
            search: state.main.search, 
            searchTerm: state.main.searchTerm, 
            users:state.main.users, 
            followers:state.main.followers, 
            following: state.main.following, 
            tracks:state.main.tracks, 
            likes: state.main.likes, 
            comments: state.main.comments, 
            commentsData: state.main.commentsData
        }
    }

    const mapDispatchToProps = (dispatch)=>{
        return {
            dispatch: dispatch
        }
    }

    return connect(mapStateToProps, mapDispatchToProps)(MainComp)
}

export default withMainStore;