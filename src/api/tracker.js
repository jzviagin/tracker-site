import axios from 'axios'



const instance = axios.create({
    baseURL: 'http://ec2-3-17-34-61.us-east-2.compute.amazonaws.com:3004/',
    //baseURL: 'http://localhost:80',
    timeout: 10000,
});

instance.interceptors.request.use(
    async (config)=>{
        const token = await localStorage.getItem('token');
        console.log('using token' , token)
        if (token){
            config.headers.Authorization = 'Bearer ' + token;
        }
        return config;
    },
    (err)=>{
        return Promise.reject('update',err);
    }
)
export default instance;